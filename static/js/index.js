async function readImage(image) {
    // see: https://dev.to/taylorbeeston/resizing-images-client-side-with-vanilla-js-4ng2

    var canvas = document.createElement('canvas')
    var img = document.createElement('img');

    img.src = await new Promise(function (resolve) {
        const reader = new FileReader();
        reader.onload = function (e) {
            resolve(e.target.result);
        };
        reader.readAsDataURL(image);
    });

    await new Promise(function (resolve) {
        img.onload = resolve;
    });

    canvas.width = img.width;
    canvas.height = img.height;
    canvas.getContext('2d').drawImage(img, 0, 0, canvas.width, canvas.height);

    return canvas;
}

const MAX_WIDTH = 512
const MAX_HEIGHT = 512
const QUALITY = 0.92

function scaleCanvas(canvas, scale) {
    // see: https://dev.to/taylorbeeston/resizing-images-client-side-with-vanilla-js-4ng2
    var scaledCanvas = document.createElement('canvas');
    scaledCanvas.width = canvas.width * scale;
    scaledCanvas.height = canvas.height * scale;

    scaledCanvas.getContext('2d').drawImage(canvas, 0, 0, scaledCanvas.width, scaledCanvas.height);
    return scaledCanvas;
}

async function downscale(image) {
    // see: https://dev.to/taylorbeeston/resizing-images-client-side-with-vanilla-js-4ng2
    let canvas = await readImage(image);

    // Half scale as long as possible to avoid interpolation artifacts
    while (canvas.width >= 2 * MAX_WIDTH || canvas.height >= 2 * MAX_HEIGHT) {
        canvas = scaleCanvas(canvas, .5)
    }

    if (canvas.width > MAX_WIDTH) {
        canvas = scaleCanvas(canvas, MAX_WIDTH / canvas.width);
    }

    if (canvas.height > MAX_HEIGHT) {
        canvas = scaleCanvas(canvas, MAX_HEIGHT / canvas.height);
    }

    return new Promise(function (resolve) {
        canvas.toBlob(resolve, 'image/jpeg', QUALITY);
    })
}

// Set image preview in input panels
$('input[type=file]').change(async function (e) {
    var [file] = this.files;
    var resized = await downscale(file);
    this.files[0] = resized;

    var url = URL.createObjectURL(resized);
    var fileURL = file && 'url(' + url + ')';

    $(this).parent().css('background-image', fileURL);
}); 

$('button').click(async function (e) {
    e.preventDefault();

    var fd = new FormData();
    var inputs = $('input[type=file]');

    // Append all files for the request
    for (var i = 0; i < inputs.length; i++) {
        if (inputs[i].files.length > 0) {
            fd.append(inputs[i].name, inputs[i].files[0]);
        }
    }

    // POST request to get the resulting style image
    $.ajax({
        url: '/',
        type: 'post',
        data: fd,
        contentType: false,
        processData: false,
    }).done(function (res) {
        $('#result').css('background-image', "url(" + res + ")");
        $('#im-link').attr('href', res);
    });
});
