import os
import tensorflow as tf
import numpy as np
import PIL.Image
import tensorflow_hub as hub

from os import listdir, makedirs
from os.path import isfile, join, exists

# Load compressed models from tensorflow_hub
os.environ['TFHUB_MODEL_LOAD_FORMAT'] = 'COMPRESSED'
hub_model = hub.load('https://tfhub.dev/google/magenta/arbitrary-image-stylization-v1-256/2')

max_dim = 512


def tensor_to_image(tensor):
    tensor = tensor * 255
    tensor = np.array(tensor, dtype=np.uint8)

    if np.ndim(tensor) > 3:
        assert tensor.shape[0] == 1
        tensor = tensor[0]

    return PIL.Image.fromarray(tensor)


def load_img(img):
    img = tf.keras.preprocessing.image.img_to_array(img)[:, :, :3] / 255.0

    shape = tf.cast(tf.shape(img)[:-1], tf.float32)
    long_dim = max(shape)
    scale = max_dim / long_dim

    new_shape = tf.cast(shape * scale, tf.int32)

    img = tf.image.resize(img, new_shape)
    img = img[tf.newaxis, :]
    return img


def gen_stylized_image(content_im, stylized_im):
    c_im = load_img(content_im)
    s_im = load_img(stylized_im)
    return hub_model(tf.constant(c_im), tf.constant(s_im))[0]


def process_images(content_im, style_im):
    res = gen_stylized_image(content_im, style_im)
    return tensor_to_image(res)
