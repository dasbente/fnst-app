from flask import Flask, render_template, request
from PIL import Image
import uuid
from fnst import process_images

app = Flask(__name__)


def save_to_path(image):
    im_name = str(uuid.uuid4().hex) + ".PNG"
    image.save("./static/images/" + im_name, "PNG")
    return "/static/images/" + im_name


def process(content_im, style_im):
    content_im = Image.open(content_im)
    style_im = Image.open(style_im)

    result_im = process_images(content_im, style_im)

    return result_im


@app.route("/")
def index():
    return render_template("index.html")


@app.route("/", methods=["POST"])
def process_image():
    content_im = request.files.get("content")
    style_im = request.files.get("style")

    result_im = process(content_im, style_im)
    path = save_to_path(result_im)

    return path, 200

